from django.contrib.auth.models import User
from django.db import models


class Message(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='author')
    recipient = models.ForeignKey(User, on_delete=models.CASCADE, related_name='recipient')
    text = models.TextField()
    text1 = models.TextField()
    created = models.DateTimeField()

    def __str__(self):
        return self.author.get_full_name() + ': ' + self.text
