from django.conf.urls import url
from django.urls import path, include
from rest_framework import routers

from messaging import views

router = routers.DefaultRouter()
router.register('users', views.UserViewSet)
router.register('messages', views.MessageViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
