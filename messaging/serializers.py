from django.contrib.auth.models import User
from rest_framework import serializers

from messaging.models import Message


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'first_name', 'last_name', 'email', 'is_staff')


class MessageSerializer(serializers.HyperlinkedModelSerializer):
    author = UserSerializer()

    class Meta:
        model = Message
        fields = ('author', 'recipient', 'text')
